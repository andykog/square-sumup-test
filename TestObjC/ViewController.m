//
//  ViewController.m
//  TestObjC
//
//  Created by Andrii Kogut on 9/21/18.
//  Copyright © 2018 Andrii Kogut. All rights reserved.
//

#import "ViewController.h"
#import <SumUp/SumUp.h>

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)cta:(id)sender {
    [SMPSumUpSDK setupWithAPIKey:@"MyAPIKey"];
    
    [SMPSumUpSDK presentLoginFromViewController:self
                                       animated:YES
                                completionBlock:nil];

    
//    // Replace with your app's callback URL.
//    NSURL *const callbackURL = [NSURL URLWithString:@"wix-test.square-pos-test://callback"];
//
//    // Specify the amount of money to charge.
//    SCCMoney *const amount = [SCCMoney moneyWithAmountCents:100 currencyCode:@"USD" error:NULL];
//
//    // Your client ID is the same as your Square Application ID.
//    // Note: You only need to set your client ID once, before creating your first request.
//    [SCCAPIRequest setClientID:@"sandbox-sq0idp-x9PVUrtFP4z3yjdHVQkXZA"];
//
//    NSError *error = nil;
//
//    SCCAPIRequest *request = [SCCAPIRequest requestWithCallbackURL:callbackURL
//                                                            amount:amount
//                                                    userInfoString:nil
//                                                        locationID:nil
//                                                             notes:@"Coffee"
//                                                        customerID:nil
//                                              supportedTenderTypes:SCCAPIRequestTenderTypeAll
//                                                 clearsDefaultFees:NO
//                                   returnAutomaticallyAfterPayment:NO
//                                                             error:&error];
//
//    if (error) {
//        NSLog(@"Error: %@", error);
//        @throw error;
//    }
//
//    [SCCAPIConnection performRequest:request error:&error];
//
//    if (error) {
//        NSLog(@"Error: %@", error);
//        @throw error;
//    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
