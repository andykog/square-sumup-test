//
//  AppDelegate.h
//  TestObjC
//
//  Created by Andrii Kogut on 9/21/18.
//  Copyright © 2018 Andrii Kogut. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <SquarePointOfSaleSDK/SquarePointOfSaleSDK.h>
//#import <SumUp/SumUp.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

