//
//  SumUp.h
//  SumUp
//

#import <UIKit/UIKit.h>

#import "SumUpSDK.h"
#import "SMPCheckoutRequest.h"
#import "SMPCheckoutResult.h"
#import "SMPSumUpSDK.h"
#import "SMPMerchant.h"
#import "SMPSkipScreenOptions.h"

//! Project version number for SumUp.
FOUNDATION_EXPORT double SumUpVersionNumber;

//! Project version string for SumUp.
FOUNDATION_EXPORT const unsigned char SumUpVersionString[];
